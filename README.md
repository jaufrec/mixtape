# mixtape
Command-line script to prepare mp3 files from media files and cue sheets.  Includes sequencing, normalizing, trimming, fading, cross-fading, and id3 tagging.

## Quick start

1. Install prerequisites.
  1. On Ubuntu: `sudo apt install sox ffmpeg lame normalize-audio eyed3`
1. Put some music files into a new directory.
1. Copy the list of music files into a cuesheet in the same directory.
  1. Either edit the provided cuesheet.csv and paste them in, one row per filename, OR
  1. copy the provided cuesheet.csv over, and then `ls >> cuesheet.csv`
1. assuming you cloned mixtape into your home directory, `python ~/mixtape/mix.py`

If it works, it should produce some .wav files in the same directory.

To use more of the features, fill out more cells in the cuesheet and repeat `mix.py`.

To see what it's doing in detail, use `mix.py --verbose --debug`

## Why you would use this
Suppose you have a bunch of music files, and you want to make a mix tape.  A nice one, with tracks that blend into each other, or at least don't have dead space.  You could manually edit each file in an audio editor, but you'd have to re-edit each file if you changed your mind.  And you'd have to learn how to use an audio editor.

With this script, you create a mixtape iteratively: create a cuesheet with all songs in the order you want, run the script, and listen to the result.  Then adjust the cuesheet however you want, from trimming an extra second here and there to reordering the tracks.  After you adjust the cuesheet, rerun the script.  Repeat until it's exactly how you want it.

More precisely, you need a directory of music files in any format ffmpeg can read, a text editor or spreadsheet to edit the cuesheet, which is a CSV file, and a command line to run the script.

## Usage
If you put mix.py in one of your PATH directories, then you can run it with simply `mix.py`, as in the examples below.  Otherwise, see the quick start above for how to run it.

`mix.py`
If all of your files and your cuesheet are in the same directory, running this command from that directory will work, and will output numbered wav files (00.wav, 01.wav, etc) into the directory.  This is ideal for adjusting; you don't have to wait for MP3 encoding, and the timing for wav files seems to be much more accurate.

`mix.py --begin=2 --end=4 --verbose`
If you are just working on a transition for a few tracks, you can speed things up a bit by only processing those tracks.  The verbose flag shows you more of what's happening.

`mix.py --album="Wednesday Special Mix" --id3tag --mp3 --output_dir=~/Wed_spec_mix --cuesheet=~/cuesheets/Wed_special.csv --source_dir=~/Music`

This command makes a mix as mp3 files, id3-tagged with artist and title from the cuesheet and a new album name from the command line, output to a specific directory, reading the cuesheet from another directory, and pulling all of the music from yet another directory.

## Command line options
Options can be in any order, and are all optional.
<table>
<tr><th>option</th><th>function</th></tr>
<tr><td>-h, --help</td><td>show this help message and exit</td></tr>
<tr><td>-a Album Name, --album=Album Name</td><td>If --id3tag is selected, use this for album title</td></tr>
<tr><td>-b STARTING_TRACK_NUM, --begin=STARTING_TRACK_NUM</td><td>Process only from this track in cuesheet</td></tr>
<tr><td>-d, --debug</td><td>execute extra, debugging instructions and reports</td></tr>
<tr><td>-e ENDING_TRACK_NUM, --end=ENDING_TRACK_NUM</td><td>Process only to this track in cuesheet</td></tr>
<tr><td>-i, --id3tag</td><td>Add ID3 information.  Uses title if present in cue sheet, or else file name.</td></tr>
<tr><td>-m, --mp3</td><td>Output MP3 files instead of wav</td></tr>
<tr><td>-o RELATIVE_PATH, --output_dir=RELATIVE_PATH</td><td>Location of output directory relative to working dir.</td></tr>
<tr><td>-p PATH, --cuesheet=PATH</td><td>Read cuesheet csv from PATH.</td></tr>
<tr><td>-r VERSION_STRING, --version_tag=VERSION_STRING</td><td>Add this version number string to the ID3 comment tag.</td></tr>
<tr><td>-s PATH, --source_dir=PATH</td><td>Path prepended to each filename in cuesheet.</td></tr>
<tr><td>-v, --verbose</td><td>Show extra messages during execution.</td></tr>
</table>
## The Cuesheet
Each mixtape requires a cuesheet.  At a bare minimum, this is a text file with the filename of each input track, one per line.  For more effects, the cuesheet can be a CSV file, with a header row, with some or all of the parameters named in the header row.

![Blue rectangle with black output area inset, labels and arrows for the five parameters in the appropriate places, and a Previous Track blue rectangle in the top left](https://gitlab.com/jaufrec/mixtape/raw/master/parameter_illustration.svg.png)

### Parameters 

* **file**  The exact filename, relative to the working directory, or to the source_dir if specified in the command line.
* **start_crop**  Remove this many seconds from the beginning of the track.
* **end_crop**  Remove this many seconds from the end of the track.
* **fadein_length**  After cropping, fade in over this many seconds.
* **fadeout_length**  After cropping, fade out over this many seconds.
* **prev_overlap**  Start this track (after cropping) with this many seconds remaining on the previous track (after cropping).  This overlaps the tracks but does not crossfade; use fadeout_length in the previous track and fadein_length in this track to crossfade.
* **title**  Used if MP3 output and ID3 tagging are specified.
* **artist**  Used if MP3 output and ID3 tagging are specified.

Seconds can be decimals.  File can be a path.

### An example cuesheet
<table>
<tr><th>file</th><th>start_crop</th><th>fadein_length</th><th>end_crop</th><th>fadeout_length</th><th>prev_overlap</th><th>artist</th><th>Title</th></tr>
<tr><td>01 - Both Hands - Ani DiFranco.mp3</td><td></td><td></td><td>6</td><td>0.5</td><td></td><td>Ani DiFranco</td><td>Both Hands</td></tr>
<tr><td>02 - Sun Comes Up It's Tuesday Morning - Cowboy Junkies.mp3</td><td></td><td></td><td>1</td><td>3</td><td>5</td><td>Cowboy Junkies</td><td>Sun Comes Up It's Tuesday Morning</td></tr>
<tr><td>03 - Angel - John Hiatt.mp3</td><td></td><td></td><td>3.5</td><td>0.3</td><td>2</td><td>John Hiatt</td><td>Angel</td></tr>
<tr><td>04 - Cry Love - John Hiatt.mp3</td><td></td><td></td><td>6</td><td></td><td>5</td><td>John Hiatt</td><td>Cry Love</td></tr>
</table>
